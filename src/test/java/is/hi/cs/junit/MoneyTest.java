package is.hi.cs.junit;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.samples.money.Money;

public class MoneyTest extends TestCase
{
	/**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MoneyTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( MoneyTest.class );
    }

    public void testAdd()
    {
    	 	Money m12 = new Money(12, "USD");
    	    Money m14 = new Money(14, "USD");
    	    Money expected = new Money(26, "USD");
    	    assertEquals(expected, m12.add(m14));
    }
    
    public void testEquals() {
    	 	Money m12 = new Money(12, "USD");
    	    Money m14 = new Money(14, "USD");
    	    Money equalMoney = new Money(12, "USD");
    	    assertTrue(m12.equals(m12));
    	    assertTrue(m12.equals(equalMoney));
    	    assertFalse(m12.equals(m14));
    }
}
