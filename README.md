# SQM Project

This is a project in Software Quality Management in the University of Iceland.

# Build automation

Maven is used for build automation

Compile project with `mvn compile` or create JAR file with `mvn package`

# Testing

JUnit 3.8.1 is used for testing.

All test case source code located in `src.test.java.is.hi.cs.junit` folder are run with `mvn test`.

[![pipeline status](https://gitlab.com/ernagudrun/sqm-project/badges/master/pipeline.svg)](https://gitlab.com/ernagudrun/sqm-project/-/commits/master)

[![coverage report](https://gitlab.com/ernagudrun/sqm-project/badges/master/coverage.svg)](https://gitlab.com/ernagudrun/sqm-project/-/commits/master)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=ernagudrun_sqm-project&metric=alert_status)](https://sonarcloud.io/dashboard?id=ernagudrun_sqm-project)
